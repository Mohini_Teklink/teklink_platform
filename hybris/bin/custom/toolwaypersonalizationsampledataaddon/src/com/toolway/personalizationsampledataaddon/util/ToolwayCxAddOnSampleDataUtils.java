/**
 *
 */
package com.toolway.personalizationsampledataaddon.util;


import de.hybris.platform.core.PK;
import de.hybris.platform.core.Registry;
import de.hybris.platform.promotionengineservices.jalo.PromotionSourceRule;
import de.hybris.platform.ruleengine.dao.RulesModuleDao;
import de.hybris.platform.ruleengine.init.RuleEngineKieModuleSwapper;
import de.hybris.platform.ruleengineservices.RuleEngineServiceException;
import de.hybris.platform.ruleengineservices.maintenance.RuleMaintenanceService;
import de.hybris.platform.servicelayer.event.events.AfterInitializationEndEvent;
import de.hybris.platform.servicelayer.event.impl.AbstractEventListener;
import de.hybris.platform.servicelayer.model.ModelService;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.Set;

import org.apache.commons.collections.CollectionUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.ApplicationContext;


/**
 * @author tarang goyal
 *
 */
public class ToolwayCxAddOnSampleDataUtils extends AbstractEventListener<AfterInitializationEndEvent>
{

	private static final Logger LOG = LoggerFactory.getLogger(ToolwayCxAddOnSampleDataUtils.class);


	public static final void storeToCompile(final PromotionSourceRule promotionSourceRule)
	{
		final Set set = Registry.getApplicationContext().getBean("personalizationsampledataaddonInitMap", Set.class);
		set.add(promotionSourceRule.getPK());
	}

	public static final boolean isRuleEngineInitailized()
	{
		final RulesModuleDao rulesModuleDao = Registry.getApplicationContext().getBean("rulesModuleDao", RulesModuleDao.class);
		return CollectionUtils.isNotEmpty(rulesModuleDao.findAll());
	}


	@Override
	protected void onEvent(final AfterInitializationEndEvent event)
	{
		final ApplicationContext context = Registry.getApplicationContext();
		final ModelService modelService = context.getBean("modelService", ModelService.class);
		final RuleMaintenanceService ruleMaintenanceService = context.getBean("ruleMaintenanceService",
				RuleMaintenanceService.class);
		final RuleEngineKieModuleSwapper ruleEngineKieModuleSwapper = context.getBean("ruleEngineKieModuleSwapper",
				RuleEngineKieModuleSwapper.class);
		final Set promotionSet = context.getBean("personalizationsampledataaddonInitMap", Set.class);
		if (CollectionUtils.isNotEmpty(promotionSet))
		{
			try
			{
				final ArrayList e = new ArrayList();
				final Iterator arg8 = promotionSet.iterator();

				while (arg8.hasNext())
				{
					final PK pk = (PK) arg8.next();
					e.add(modelService.get(pk));
				}

				ruleEngineKieModuleSwapper.waitForSwappingToFinish();
				ruleMaintenanceService.compileAndPublishRules(e);
				ruleEngineKieModuleSwapper.waitForSwappingToFinish();
				LOG.info("CxAddOnSampleDataUtils done for {}", promotionSet);
			}
			catch (final RuleEngineServiceException arg9)
			{
				LOG.error("Promotion compilation failed", arg9);
			}
			catch (final Exception arg10)
			{
				LOG.error("Unexpected error", arg10);
			}
		}
		else
		{
			LOG.info("No personalized promotions to compile from sampledata.");
		}

	}


}
